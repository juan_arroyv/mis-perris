from django.conf.urls import include , url
from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('contactanos', views.contacto, name='contacto'),
    path('lista-usuarios', views.listar, name ='lista'),
    path('lista-mascota', views.masclist, name='masclist'),
    path('mascota', views.crea_mascotas, name='crea_mascotas'),
    path('update/<int:id>/',views.upd_mascota , name="upd_mascota"),
    path('delete/<int:id>/', views.del_mascota, name="del_mascota"),
     
    path('Mascota/rescatados', views.listar_resc, name='rescatado'),
    path('Mascota/adoptados', views.listar_adop, name='adoptados'),
    path('Mascota/disponibles', views.listar_disp, name='disponibles'),
    path('Servicios', views.servicio, name="servicios")

    
]