from django.apps import AppConfig


class AppPerrisConfig(AppConfig):
    name = 'app_perris'
