from django.shortcuts import render, HttpResponsePermanentRedirect, redirect
from django.template import RequestContext
from .models import Usuario, Mascota
from .forms import RegFormulario, MascotaFormulario
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

# Create your views here.

def inicio(request):
    return render(request, 'perris/index.html')


@login_required(login_url="/accounts/login/")
def contacto(request):

    if request.method == 'POST':
        form = RegFormulario(request.POST)
        if form.is_valid():
            form.save()
          
            return  redirect('servicios')
    else:
        form = RegFormulario()
        return render(request, 'perris/formulario.html', {'form': form})
        

@login_required(login_url="/accounts/login/")
def servicio(request):
    return render(request, 'perris/servicio.html')


@login_required(login_url="/accounts/login/")
def listar(request):
    Usuarios = Usuario.objects.all().orderby('nombre')
    return render(request, 'perris/listausuarios.html', {'lista': Usuarios})


@login_required(login_url="/cuentauser/login/")
def crea_mascotas(request):
    if request.method == "POST":
        form = MascotaFormulario(request.POST, request.FILES)
        print(form.errors)
        if form.is_valid():
            form.save()
            mascotas = Mascota.objects.all()
            return render(request, 'perris/mascotalista.html', {'listam': mascotas})
    else:
        form = MascotaFormulario()
        return render(request, 'perris/formulariomascota.html', {'form': form})


@login_required(login_url="/accounts/login/")
def masclist(request):
    mascotas = Mascota.objects.all()
    return render(request, 'perris/mascotalista.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def upd_mascota(request, id):
    mascota = Mascota.objects.get(id=id)
    form = MascotaFormulario(request.POST or None, instance=mascota)
    print(mascota.nombre)
    if form.is_valid():
        form.save()
        mascotas = Mascota.objects.all()
        return render(request, 'perris/mascotalista.html', {'listam': mascotas})
    
    return render(request, 'perris/formulariomascota.html', {'mascota': mascota}) 

@login_required(login_url="/accounts/login/")
def del_mascota(request, id):
    mascota = Mascota.objects.get(id=id)

    if request.method == 'POST':
        mascota.delete()
        mascotas = Mascota.objects.all()
        return render(request, 'perris/mascotalista.html', {'listam': mascotas})

    return render(request, 'perris/confirmacion.html', {'mascota': mascota})

@login_required(login_url="/accounts/login/")
def listar_resc(request):
    mascotas = Mascota.objects.all().filter(estado='Rescatado')
    return render(request, 'perris/mascotasrescatados.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def listar_adop(request):
    mascotas = Mascota.objects.all().filter(estado='Adoptado')
    return render(request, 'perris/mascotaadoptados.html', {'listam': mascotas})

@login_required(login_url="/accounts/login/")
def listar_disp(request):
    mascotas = Mascota.objects.all().filter(estado='Disponible')
    return render(request, 'perris/mascotadisponibles.html', {'listam': mascotas})


