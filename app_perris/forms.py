from django import forms

from .models import Usuario, Mascota

class RegFormulario(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = (
                  'correo', 
                  'rut', 
                  'nombre', 
                  'fechaNacimiento', 
                  'telefono', 
                  'region', 
                  'ciudad', 
                  'tipoCasa'
                  )


class MascotaFormulario(forms.ModelForm):
    fotografia = forms.ImageField()
    class Meta:
        model = Mascota
        fields = (
            'fotografia',
            'nombre',
            'razaPredominante',
            'descripcion',
            'estado',
        )
